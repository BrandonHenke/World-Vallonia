# **Background**

## **The Invariants**
> The Warper (spacetime)  
> The Inviting (electroweak + strong unification)  
> The Crendenter (quantum mechanics)  
> The Mystic (magical)  

The universe was created.
The Invariants were created out of Order during the shattering.

### **Interactions:**
> W+I = the weird  
> W+C = the cosmos  
> **W+M** = spacetime warping (tempórimancy+spátimancy)  
> I+C = Chaos (StatM + ClassM)  
> **I+M** = light (lúcimancy) + matter manipulation (matérimancy)  
> **C+M** = magical probability manipulation (fortúnimancy)  

## **Invariant Constructs**

#### **The Warper and the Mystic**

The Warper and the Mystic created the different planes of existance, outside of the physical one.
Examples include:
> The "Hub World"